const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res) => {
  res.send('Hellow');
});

router.get('/:id', (req, res) => {
  console.log(req.params.id);
  res.send(`ID`);
});
// createUserValid
router.post('/', responseMiddleware, (req, res) => {
  // console.log(req.body);
  // createUserValid(req, res, () => {});
  res.send('Hello putw');
});

router.put('/:id', (req, res) => {
  console.log(req.params.id);
  res.send(`ID Put`);
});

router.delete('/:id', (req, res) => {
  console.log(req.params.id);
  res.send(`ID delete`);
});

// TODO: Implement route controllers for user

module.exports = router;
