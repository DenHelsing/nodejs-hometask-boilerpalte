const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res) => {
  res.send('Hellow');
});

router.get('/:id', (req, res) => {
  console.log(req.params.id);
  res.send(`ID`);
});

router.post('/', (req, res) => {
  res.send('Hello putw');
});

router.put('/:id', (req, res) => {
  console.log(req.params.id);
  res.send(`ID Put`);
});

router.delete('/:id', (req, res) => {
  console.log(req.params.id);
  res.send(`ID delete`);
});
// TODO: Implement route controllers for fighter

module.exports = router;
