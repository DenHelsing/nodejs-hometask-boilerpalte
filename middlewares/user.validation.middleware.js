const { user } = require('../models/user');

const validateInput = (data) => {
  if (
    Object.keys(data).length === 0 ||
    !data.email.match(/^[0-9a-zA-Z\.]+@gmail.com$/g) ||
    !data.phoneNumber.match(/^\+380[0-9]{9}$/g) ||
    data.password < 3 ||
    !data.firstName ||
    !data.lastName
  ) {
    return false;
  }
  Object.keys(data).forEach((key) => {
    if (!Object.keys(user).includes(key)) {
      delete data[key];
    }
    console.log(key);
  });
  if (Object.keys(data).includes('id')) delete [id];

  return true;
};

const createUserValid = (req, res, next) => {
  if (validateInput(req.body)) {
    console.log(req.body);
  } else {
    res.status(422).send();
  }

  // Jesus, seems like i'm not good at backend :D

  // TODO: Implement validatior for user entity during creation
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
